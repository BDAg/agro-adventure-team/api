const mongoose = require('mongoose');

const feedbackSchema = new mongoose.Schema({
  player_id: String,
  message: String,
  rate_stars: Number,
  mission_id: String,
  created_date: {
    type:Date,
    default:Date.now()
  }
});

module.exports = mongoose.model('feedback',feedbackSchema);
