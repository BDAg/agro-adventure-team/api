const express = require('express');
const router = express.Router();

const controller = require('../controllers/feedbackController');

router.get('/', controller.show);
router.post('/', controller.create);
router.delete('/', controller.destroy);
router.put('/', controller.edit);
module.exports = router;
