const feedbackDB = require('../models/feedbackModel');

module.exports = {
  async create(req, res){
    const { player_id, mission_id, message, rate_stars } = req.body;

    await feedbackDB.create({
      player_id,
      mission_id,
      message,
      rate_stars
    }, (err, callback) => {
      if(err) {
        return res.status(500).send({err: err});
      }
      return res.status(200).send({message: 'Inserted feedback', callback})
    })
  },
  async show(req, res){
    await feedbackDB.find({}, (err, callback) => {
      if(err) {
        return res.status(500).send({err: err});
      }
      return res.status(200).send(callback)
    })
  },
  async destroy(req, res){
    const { _id } = req.body;
    await feedbackDB.deleteOne({'_id':_id}, (err, callback) => {
      if(err) {
        return res.status(500).send({err: err});
      }
      return res.status(200).send({message: `Mensagem de id ${_id} deletada.`})
    })
  },
  async edit(req, res){
    const { _id, message, rate_stars } = req.body;
    await feedbackDB.updateOne({'_id':_id}, {
      message,
      rate_stars
    }, (err, callback) => {
      if(err) {
        return res.status(500).send({err: err});
      }
      return res.status(200).send({message: `Mensagem de id ${_id} editada.`,callback})
    })
  }
}
